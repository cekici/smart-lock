Smart Lock Web App
===============
Single page web frontend application for Smart Lock.

Node v8.x & npm v5.x is required

## How To Install
```
$ npm install
```

## How To Run

```
$ npm run start
```

Then in your browser navigate to [http://localhost:8080](http://localhost:8080) for the Smart Lock App.

## How To Use
Initially app redirects you to login screen. You can login with the following credentials:
`username`: `admin` and `password`: `123`.

If the authenticated user has staff role, she only have dashboard page, with list of door buttons. Clicking on any button means, she tries to open the door. Either access granted or access error message will be shown to the user.
If the authenticated user has admin role, she have the dashboard page as well as users, doors and logs pages.
On users page, she can either create a new user or click on any user will display the list of doors & authorization status of the related user to the doors.
On doors page she can either create a new door or click on any door will display the list of users & authorization status of them to the related door.
On logs page, all the door opening attempts are listed.


The app has a responsive UI.

