import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as userActions from '../../core/user/userActions';
import * as doorActions from '../../core/door/doorActions';
import * as logActions from '../../core/log/logActions';
import SLLogList from '../../components/log/list/SLLogListContainer';

import './_sl-logs-page-content.scss';

class SmartSLLogsPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetchLogsFinished: false,
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(userActions.getUsers());
    dispatch(doorActions.getDoors());
    dispatch(logActions.getLogs());
  }

  componentWillReceiveProps(nextProps) {
    const { hasActiveGetLogsAction } = this.props;

    if (!nextProps.hasActiveGetLogsAction && hasActiveGetLogsAction) {
      this.setState({
        isFetchLogsFinished: true,
      });
    }
  }

  render() {
    const { logs, users, doors } = this.props;
    const { isFetchLogsFinished } = this.state;

    return (
      <div className="sl-logs-page">
        <SLLogList
          isFetchFinished={isFetchLogsFinished}
          logs={logs}
          users={users}
          doors={doors}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    authenticationState: {
      profile,
    },
    logState: {
      logs,
      hasActiveGetLogsAction,
    },
    userState: {
      users,
    },
    doorState: {
      doors,
    },
  } = state;

  return {
    profile,
    logs,
    users,
    doors,
    hasActiveGetLogsAction,
  };
}

const ConnectedSmartSLLogsPageContent = connect(mapStateToProps)(SmartSLLogsPageContent);
const ConnectedSmartSLLogsPageContentWithRouter = withRouter(ConnectedSmartSLLogsPageContent);

export default ConnectedSmartSLLogsPageContentWithRouter;
