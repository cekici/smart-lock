import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import isEmpty from 'lodash/isEmpty';
import classNames from 'classnames';

import * as doorActions from '../../core/door/doorActions';
import SLDoorButton from '../../components/door/button/SLDoorButton';
import SLButton from '../../components/button/SLButton';

import './_sl-dashboard-page-content.scss';

class SmartSLDashboardPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
      modalText: '',
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(doorActions.getDoors());
  }

  componentWillReceiveProps(nextProps) {
    const { hasActiveOpenDoorAction } = this.props;

    if (!nextProps.hasActiveOpenDoorAction && hasActiveOpenDoorAction) {
      if (isEmpty(nextProps.doorApiErrors)) {
        this.setState({
          isModalOpen: true,
          modalText: 'Access Granted',
          isOpenDoorSuccess: true,
        });
      } else {
        this.setState({
          isModalOpen: true,
          modalText: 'Access Denied',
          isOpenDoorSuccess: false,
        });
      }
    }
  }

  handleOpenDoorClick = (id) => {
    const { dispatch } = this.props;
    dispatch(doorActions.openDoor({ id }));
  }

  handleCloseModal = () => {
    this.setState({
      isModalOpen: false,
    });
  }

  render() {
    const {
      profile,
      doors,
      hasActiveOpenDoorAction,
      candidateDoorIdToOpen,
    } = this.props;

    const {
      isModalOpen,
      modalText,
      isOpenDoorSuccess,
    } = this.state;

    const profileFullName = `${profile.firstName} ${profile.lastName}`;
    let doorName = '';

    if (candidateDoorIdToOpen) {
      const door = doors.find(item => item.id === candidateDoorIdToOpen);
      doorName = door ? door.name : '';
    }

    const modalTextClassName = classNames('sl-modal-text', {
      'is-success': isOpenDoorSuccess,
      'is-error': !isOpenDoorSuccess,
    });

    const modalButtonClassName = classNames('sl-modal-close-button', {
      'is-success': isOpenDoorSuccess,
      'is-error': !isOpenDoorSuccess,
    });

    return (
      <div className="sl-dashboard-page">
        <h6 className="sl-dashboard-welcome-title">{`Welcome, ${profileFullName}.`}</h6>

        {doors.length ?
          <h6 className="sl-dashboard-welcome-title">
            {'Click on a door below to open:'}
          </h6> : null
        }

        {doors.length ?
          <ul className="sl-dashboard-door-list">
            {doors.map(door =>
              (<SLDoorButton
                door={door}
                key={door.id}
                hasActiveOpenDoorAction={hasActiveOpenDoorAction}
                candidateDoorIdToOpen={candidateDoorIdToOpen}
                onClick={this.handleOpenDoorClick}
              />
              ))}
          </ul> : null
        }

        <ReactModal
          isOpen={isModalOpen}
          closeTimeoutMS={200}
          shouldCloseOnOverlayClick
        >

          <p className={modalTextClassName}>{`${modalText} to ${doorName}`}</p>

          <SLButton
            onClick={this.handleCloseModal}
            className={modalButtonClassName}
            text="Close"
          />
        </ReactModal>

      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    authenticationState: {
      profile,
    },
    doorState: {
      doors,
      hasActiveOpenDoorAction,
      candidateDoorIdToOpen,
      doorApiErrors,
    },
  } = state;

  return {
    profile,
    doors,
    hasActiveOpenDoorAction,
    candidateDoorIdToOpen,
    doorApiErrors,
  };
}

const ConnectedSmartSLDashboardPageContent = connect(mapStateToProps)(SmartSLDashboardPageContent);
const ConnectedSmartSLDashboardPageContentWithRouter = withRouter(ConnectedSmartSLDashboardPageContent);

export default ConnectedSmartSLDashboardPageContentWithRouter;
