import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as authenticationActions from '../../core/authentication/authenticationActions';
import SLLoginForm from '../../components/login/form/SLLoginForm';

import './_sl-login-page-content.scss';

class SmartSLLoginPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formInitialValueMap: {
        username: '',
        password: '',
      },
      errorMap: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    const { authenticationApiErrors } = this.props;

    if (nextProps.authenticationApiErrors !== authenticationApiErrors) {
      this.setState({
        errorMap: nextProps.authenticationApiErrors,
      });
    }
  }

  handleFormSubmit = (valueMap, validation) => {
    const { dispatch } = this.props;

    if (!validation.username && !validation.password) {
      dispatch(authenticationActions.login(valueMap));
    } else {
      this.setState({
        errorMap: validation,
      });
    }
  }

  render() {
    const {
      hasActiveLoginAction,
    } = this.props;

    const {
      formInitialValueMap,
      errorMap,
    } = this.state;

    return (
      <div className="sl-login-page">
        <SLLoginForm
          errorMap={errorMap}
          formInitialValueMap={formInitialValueMap}
          hasActiveLoginAction={hasActiveLoginAction}
          onSubmit={this.handleFormSubmit}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    authenticationState: {
      profile,
      authenticationApiErrors,
      hasActiveLoginAction,
      isTokenValid,
    },
  } = state;

  return {
    profile,
    authenticationApiErrors,
    hasActiveLoginAction,
    isTokenValid,
  };
}

const ConnectedSmartSLLoginPageContent = connect(mapStateToProps)(SmartSLLoginPageContent);
const ConnectedSmartSLLoginPageContentWithRouter = withRouter(ConnectedSmartSLLoginPageContent);

export default ConnectedSmartSLLoginPageContentWithRouter;
