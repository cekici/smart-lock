import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as userActions from '../../core/user/userActions';
import * as doorActions from '../../core/door/doorActions';
import * as authorizationActions from '../../core/authorization/authorizationActions';
import SLAuthorizationListContainer from '../../components/authorization/list-container/SLAuthorizationListContainer';

import './_sl-door-page-content.scss';

class SmartSLDoorPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.match.params.id,
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    const { id } = this.state;

    dispatch(doorActions.getDoor({ id }));
    dispatch(userActions.getUsers());
    dispatch(authorizationActions.getAuthorizations({ door: id }));
  }

  handleChangeAuthorizationClick = (id, status) => {
    const { dispatch } = this.props;

    dispatch(authorizationActions.updateAuthorization({ id, status }));
  }

  render() {
    const {
      door,
      users,
      authorizations,
      hasActiveUpdateAuthorizationAction,
      candidateUpdateAuthorizationId,
    } = this.props;

    const {
      name,
    } = door;

    return (
      <div className="sl-door-page">
        <div className="sl-door-page-profile">
          <div className="sl-door-page-profile-header">
            <span className="sl-door-page-profile-header-name">Name</span>
          </div>
          <div className="sl-door-page-profile-content">
            <span className="sl-door-page-profile-content-name">{name}</span>
          </div>
        </div>

        <SLAuthorizationListContainer
          authorizations={authorizations}
          door={door}
          users={users}
          hasActiveUpdateAuthorizationAction={hasActiveUpdateAuthorizationAction}
          candidateUpdateAuthorizationId={candidateUpdateAuthorizationId}
          onChangeAuthorizationClick={this.handleChangeAuthorizationClick}
        />


      </div>
    );
  }
}

function

mapStateToProps(state) {
  const {
    doorState: {
      door,
    },
    userState: {
      users,
    },
    authorizationState: {
      authorizations,
      hasActiveGetAuthorizationsAction,
      hasActiveUpdateAuthorizationAction,
      candidateUpdateAuthorizationId,
    },
  } = state;

  return {
    door,
    users,
    authorizations,
    hasActiveGetAuthorizationsAction,
    hasActiveUpdateAuthorizationAction,
    candidateUpdateAuthorizationId,
  };
}

const ConnectedSmartSLDoorPageContent = connect(mapStateToProps)(SmartSLDoorPageContent);
const ConnectedSmartSLDoorPageContentWithRouter = withRouter(ConnectedSmartSLDoorPageContent);
export default ConnectedSmartSLDoorPageContentWithRouter;
