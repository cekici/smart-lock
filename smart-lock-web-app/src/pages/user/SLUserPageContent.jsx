import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as userActions from '../../core/user/userActions';
import * as doorActions from '../../core/door/doorActions';
import * as authorizationActions from '../../core/authorization/authorizationActions';
import SLAuthorizationListContainer from '../../components/authorization/list-container/SLAuthorizationListContainer';

import './_sl-user-page-content.scss';

class SmartSLUserPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.match.params.id,
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    const { id } = this.state;

    dispatch(userActions.getUser({ id }));
    dispatch(doorActions.getDoors());
    dispatch(authorizationActions.getAuthorizations({ user: id }));
  }

  handleChangeAuthorizationClick = (id, status) => {
    const { dispatch } = this.props;

    dispatch(authorizationActions.updateAuthorization({ id, status }));
  }

  render() {
    const {
      user,
      doors,
      authorizations,
      hasActiveUpdateAuthorizationAction,
      candidateUpdateAuthorizationId,
    } = this.props;

    const {
      firstName,
      lastName,
      username,
      role,
    } = user;

    const userName = `${firstName} ${lastName}`;

    return (
      <div className="sl-user-page">
        <div className="sl-user-page-profile">
          <div className="sl-user-page-profile-header">
            <span className="sl-user-page-profile-header-name">Name</span>
            <span className="sl-user-page-profile-header-username">Username</span>
            <span className="sl-user-page-profile-header-role">Role</span>
          </div>
          <div className="sl-user-page-profile-content">
            <span className="sl-user-page-profile-content-name">{userName}</span>
            <span className="sl-user-page-profile-content-username">{username}</span>
            <span className="sl-user-page-profile-content-role">{role}</span>
          </div>
        </div>

        <SLAuthorizationListContainer
          authorizations={authorizations}
          user={user}
          doors={doors}
          hasActiveUpdateAuthorizationAction={hasActiveUpdateAuthorizationAction}
          candidateUpdateAuthorizationId={candidateUpdateAuthorizationId}
          onChangeAuthorizationClick={this.handleChangeAuthorizationClick}
        />


      </div>
    );
  }
}

function

mapStateToProps(state) {
  const {
    userState: {
      user,
    },
    doorState: {
      doors,
    },
    authorizationState: {
      authorizations,
      hasActiveGetAuthorizationsAction,
      hasActiveUpdateAuthorizationAction,
      candidateUpdateAuthorizationId,
    },
  } = state;

  return {
    user,
    doors,
    authorizations,
    hasActiveGetAuthorizationsAction,
    hasActiveUpdateAuthorizationAction,
    candidateUpdateAuthorizationId,
  };
}

const ConnectedSmartSLUserPageContent = connect(mapStateToProps)(SmartSLUserPageContent);
const ConnectedSmartSLUserPageContentWithRouter = withRouter(ConnectedSmartSLUserPageContent);
export default ConnectedSmartSLUserPageContentWithRouter;
