import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';

import * as userActions from '../../core/user/userActions';
import SLUserListContainer from '../../components/user/list-container/SLUserListContainer';
import SLUserForm from '../../components/user/form/SLUserForm';
import SLButton from '../../components/button/SLButton';

import './_sl-users-page-content.scss';

class SmartSLUsersPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetchUsersFinished: false,
      displayMode: 'list',
      formInitialValueMap: {
        firstName: '',
        lastName: '',
        username: '',
      },
      errorMap: {},
    };
  }


  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(userActions.getUsers());
  }

  componentWillReceiveProps(nextProps) {
    const { hasActiveGetUsersAction, hasActiveCreateUserAction, userApiErrors } = this.props;

    if (!nextProps.hasActiveGetUsersAction && hasActiveGetUsersAction) {
      this.setState({
        isFetchUsersFinished: true,
      });
    }

    if (!nextProps.hasActiveCreateUserAction && hasActiveCreateUserAction && isEmpty(nextProps.userApiErrors)) {
      this.setState({
        displayMode: 'list',
      });
    }

    if (nextProps.userApiErrors !== userApiErrors) {
      this.setState({
        errorMap: nextProps.userApiErrors,
      });
    }
  }

  handleChangeDisplayModeClick = () => {
    const { displayMode } = this.state;
    let newDisplayMode;

    if (displayMode === 'list') {
      newDisplayMode = 'form';
    } else if (displayMode === 'form') {
      newDisplayMode = 'list';
    }

    this.setState({
      displayMode: newDisplayMode,
    });
  }

  handleFormSubmit = (valueMap, validation) => {
    const { dispatch } = this.props;

    if (!validation.firstName && !validation.lastName && !validation.username) {
      dispatch(userActions.createUser(valueMap));
    } else {
      this.setState({
        errorMap: validation,
      });
    }
  }

  handleClick = (userId) => {
    const { history } = this.props;

    history.push(`/user/${userId}`);
  }

  render() {
    const {
      users,
      hasActiveCreateUserAction,
    } = this.props;
    const {
      displayMode,
      errorMap,
      formInitialValueMap,
      isFetchUsersFinished,
    } = this.state;

    return (
      <div className="sl-users-page">
        <SLButton
          type="button"
          className="sl-users-page-change-display-mode-button"
          text={displayMode === 'list' ? 'Create a New User' : 'Back to List'}
          onClick={this.handleChangeDisplayModeClick}
        />

        {displayMode === 'form' &&
        <SLUserForm
          errorMap={errorMap}
          formInitialValueMap={formInitialValueMap}
          hasActiveCreateAction={hasActiveCreateUserAction}
          onSubmit={this.handleFormSubmit}
        />
        }

        {displayMode === 'list' &&
        <SLUserListContainer
          isFetchFinished={isFetchUsersFinished}
          users={users}
          onClick={this.handleClick}
        />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    userState: {
      users,
      hasActiveGetUsersAction,
      hasActiveCreateUserAction,
      userApiErrors,
    },
  } = state;

  return {
    users,
    hasActiveGetUsersAction,
    hasActiveCreateUserAction,
    userApiErrors,
  };
}

const ConnectedSmartSLUsersPageContent = connect(mapStateToProps)(SmartSLUsersPageContent);
const ConnectedSmartSLUsersPageContentWithRouter = withRouter(ConnectedSmartSLUsersPageContent);

export default ConnectedSmartSLUsersPageContentWithRouter;
