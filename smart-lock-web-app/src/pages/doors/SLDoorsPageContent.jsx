import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';

import * as doorActions from '../../core/door/doorActions';
import SLDoorListContainer from '../../components/door/list-container/SLDoorListContainer';
import SLDoorForm from '../../components/door/form/SLDoorForm';
import SLButton from '../../components/button/SLButton';

import './_sl-doors-page-content.scss';

class SmartSLDoorsPageContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetchDoorsFinished: false,
      displayMode: 'list',
      formInitialValueMap: {
        name: '',
      },
      errorMap: {},
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(doorActions.getDoors());
  }

  componentWillReceiveProps(nextProps) {
    const { hasActiveGetDoorsAction, hasActiveCreateDoorAction, doorApiErrors } = this.props;

    if (!nextProps.hasActiveGetDoorsAction && hasActiveGetDoorsAction) {
      this.setState({
        isFetchDoorsFinished: true,
      });
    }

    if (!nextProps.hasActiveCreateDoorAction && hasActiveCreateDoorAction && isEmpty(nextProps.doorApiErrors)) {
      this.setState({
        displayMode: 'list',
      });
    }

    if (nextProps.doorApiErrors !== doorApiErrors) {
      this.setState({
        errorMap: nextProps.doorApiErrors,
      });
    }
  }

  handleChangeDisplayModeClick = () => {
    const { displayMode } = this.state;
    let newDisplayMode;

    if (displayMode === 'list') {
      newDisplayMode = 'form';
    } else if (displayMode === 'form') {
      newDisplayMode = 'list';
    }

    this.setState({
      displayMode: newDisplayMode,
    });
  }

  handleFormSubmit = (valueMap, validation) => {
    const { dispatch } = this.props;

    if (!validation.name) {
      dispatch(doorActions.createDoor(valueMap));
    } else {
      this.setState({
        errorMap: validation,
      });
    }
  }


  handleClick = (doorId) => {
    const { history } = this.props;

    history.push(`/door/${doorId}`);
  }

  render() {
    const {
      doors,
      hasActiveCreateDoorAction,
    } = this.props;
    const {
      displayMode,
      errorMap,
      formInitialValueMap,
      isFetchDoorsFinished,
    } = this.state;

    return (
      <div className="sl-doors-page">
        <SLButton
          type="button"
          className="sl-doors-page-change-display-mode-button"
          text={displayMode === 'list' ? 'Create a New Door' : 'Back to List'}
          onClick={this.handleChangeDisplayModeClick}
        />

        {displayMode === 'form' &&
        <SLDoorForm
          errorMap={errorMap}
          formInitialValueMap={formInitialValueMap}
          hasActiveCreateAction={hasActiveCreateDoorAction}
          onSubmit={this.handleFormSubmit}
        />
        }

        {displayMode === 'list' &&
        <SLDoorListContainer
          isFetchFinished={isFetchDoorsFinished}
          doors={doors}
          onClick={this.handleClick}
        />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    doorState: {
      doors,
      hasActiveGetDoorsAction,
      hasActiveCreateDoorAction,
      doorApiErrors,
    },
  } = state;

  return {
    doors,
    hasActiveGetDoorsAction,
    hasActiveCreateDoorAction,
    doorApiErrors,
  };
}

const ConnectedSmartSLDoorsPageContent = connect(mapStateToProps)(SmartSLDoorsPageContent);
const ConnectedSmartSLDoorsPageContentWithRouter = withRouter(ConnectedSmartSLDoorsPageContent);

export default ConnectedSmartSLDoorsPageContentWithRouter;
