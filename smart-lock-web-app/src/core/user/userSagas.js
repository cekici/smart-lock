import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../constants/actionTypes';
import {
  getUsersFulfilled,
  getUsersRejected,
  createUserFulfilled,
  createUserRejected,
  getUserFulfilled,
  getUserRejected,
} from './userActions';
import userApi from './userApi';

function* getUsers() {
  try {
    const users = yield call(userApi.getUsers);
    yield put(getUsersFulfilled(users));
  } catch (e) {
    yield put(getUsersRejected(e));
  }
}

function* createUser(action) {
  try {
    const profile = yield call(userApi.createUser, action.payload);
    yield put(createUserFulfilled(profile));
  } catch (e) {
    yield put(createUserRejected(e));
  }
}

function* getUser(action) {
  try {
    const user = yield call(userApi.getUser, action.payload);
    yield put(getUserFulfilled(user));
  } catch (e) {
    yield put(getUserRejected(e));
  }
}

const userSagas = [
  takeEvery(ACTIONS.GET_USERS.base, getUsers),
  takeEvery(ACTIONS.CREATE_USER.base, createUser),
  takeEvery(ACTIONS.GET_USER.base, getUser),
];

export default userSagas;
