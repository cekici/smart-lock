import * as ACTIONS from '../../constants/actionTypes';

export const getUsers = () => ({
  type: ACTIONS.GET_USERS.base,
});

export const getUsersFulfilled = response => ({
  type: ACTIONS.GET_USERS.fulfilled,
  payload: response,
});

export const getUsersRejected = error => ({
  type: ACTIONS.GET_USERS.rejected,
  payload: error,
  isError: true,
});

export const createUser = params => ({
  type: ACTIONS.CREATE_USER.base,
  payload: params,
});

export const createUserFulfilled = response => ({
  type: ACTIONS.CREATE_USER.fulfilled,
  payload: response,
});

export const createUserRejected = error => ({
  type: ACTIONS.CREATE_USER.rejected,
  payload: error,
  isError: true,
});

export const getUser = params => ({
  type: ACTIONS.GET_USER.base,
  payload: params,
});

export const getUserFulfilled = response => ({
  type: ACTIONS.GET_USER.fulfilled,
  payload: response,
});

export const getUserRejected = error => ({
  type: ACTIONS.GET_USER.rejected,
  payload: error,
  isError: true,
});
