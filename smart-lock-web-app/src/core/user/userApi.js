import networkManager from '../../common/network/networkManager';

const userApi = {
  getUsers() {
    return networkManager.api.get('/users');
  },

  createUser(params) {
    return networkManager.api.post('/user', params);
  },

  getUser(params) {
    return networkManager.api.get(`/user/${params.id}`);
  },
};

export default userApi;
