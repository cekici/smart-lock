import * as ACTIONS from '../../constants/actionTypes';

const defaultInitialState = {
  users: [],
  user: {},
  userApiErrors: {},
  hasActiveGetUsersAction: false,
  hasActiveCreateUserAction: false,
};

const defaultMessage = 'Oops! Something is broken';

function UserReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.GET_USERS.base: {
        newState = {
          ...state,
          hasActiveGetUsersAction: true,
        };
        break;
      }

      case ACTIONS.GET_USERS.fulfilled: {
        const users = action.payload.data;

        newState = {
          ...state,
          users,
          hasActiveGetUsersAction: false,
        };

        break;
      }

      case ACTIONS.GET_USERS.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          userApiErrors: error,
          hasActiveGetUsersAction: false,
        };

        break;
      }

      case ACTIONS.CREATE_USER.base: {
        newState = {
          ...state,
          hasActiveCreateUserAction: true,
        };
        break;
      }


      case ACTIONS.CREATE_USER.fulfilled: {
        const { user } = action.payload.data;
        const { users } = state;


        newState = {
          ...state,
          users: [
            ...users,
            user,
          ],
          userApiErrors: {},
          hasActiveCreateUserAction: false,
        };

        break;
      }

      case ACTIONS.CREATE_USER.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          userApiErrors: error,
          hasActiveCreateUserAction: false,
        };

        break;
      }

      case ACTIONS.GET_USER.fulfilled: {
        const user = action.payload.data;

        newState = {
          ...state,
          user,
          hasActiveGetUsersAction: false,
        };

        break;
      }

      case ACTIONS.GET_USER.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          userApiErrors: error,
          hasActiveGetUsersAction: false,
        };

        break;
      }


      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default UserReducer;
