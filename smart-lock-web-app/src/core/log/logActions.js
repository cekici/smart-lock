import * as ACTIONS from '../../constants/actionTypes';

export const getLogs = () => ({
  type: ACTIONS.GET_LOGS.base,
});

export const getLogsFulfilled = response => ({
  type: ACTIONS.GET_LOGS.fulfilled,
  payload: response,
});

export const getLogsRejected = error => ({
  type: ACTIONS.GET_LOGS.rejected,
  payload: error,
  isError: true,
});
