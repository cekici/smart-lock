import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../constants/actionTypes';
import {
  getLogsFulfilled,
  getLogsRejected,
} from './logActions';
import logApi from './logApi';

function* getLogs() {
  try {
    const logs = yield call(logApi.getLogs);
    yield put(getLogsFulfilled(logs));
  } catch (e) {
    yield put(getLogsRejected(e));
  }
}

const logSagas = [
  takeEvery(ACTIONS.GET_LOGS.base, getLogs),
];

export default logSagas;
