import * as ACTIONS from '../../constants/actionTypes';

const defaultInitialState = {
  logs: [],
  logApiErrors: {},
  hasActiveGetLogsAction: false,
};

const defaultMessage = 'Oops! Something is broken';

function LogReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.GET_LOGS.base: {
        newState = {
          ...state,
          hasActiveGetLogsAction: true,
        };
        break;
      }


      case ACTIONS.GET_LOGS.fulfilled: {
        const logs = action.payload.data.reverse();

        newState = {
          ...state,
          logs,
          hasActiveGetLogsAction: false,
        };

        break;
      }

      case ACTIONS.GET_LOGS.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          logApiErrors: error,
          hasActiveGetLogsAction: false,
        };

        break;
      }


      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default LogReducer;
