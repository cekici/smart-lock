import * as ACTIONS from '../../constants/actionTypes.js';
import LogReducer from './LogReducer.js';

const initialState = {
  logs: [],
  logApiErrors: {},
  hasActiveGetLogsAction: false,
};


describe('Log Reducer', () => {
  const reducer = new LogReducer();

  it('should return the initial state', () => {
    expect(reducer(undefined, {}))
      .toEqual(initialState);
  });
});
