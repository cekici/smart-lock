import * as ACTIONS from '../../constants/actionTypes.js';
import * as logActions from './logActions.js';

describe('actions', () => {
  it('should create an action to get logs', () => {
    const expectedAction = {
      type: ACTIONS.GET_LOGS.base,
    };
    expect(logActions.getLogs()).toEqual(expectedAction);
  });
});
