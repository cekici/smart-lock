import networkManager from '../../common/network/networkManager';

const logApi = {
  getLogs() {
    return networkManager.api.get('/logs');
  },
};

export default logApi;
