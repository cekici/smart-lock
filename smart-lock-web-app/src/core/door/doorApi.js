import networkManager from '../../common/network/networkManager';

const doorApi = {
  getDoors() {
    return networkManager.api.get('/doors');
  },

  createDoor(params) {
    return networkManager.api.post('/door', params);
  },

  getDoor(params) {
    return networkManager.api.get(`/door/${params.id}`);
  },

  openDoor(params) {
    return networkManager.api.post(`/door/${params.id}/open`);
  },
};

export default doorApi;
