import * as ACTIONS from '../../constants/actionTypes';

export const getDoors = () => ({
  type: ACTIONS.GET_DOORS.base,
});

export const getDoorsFulfilled = response => ({
  type: ACTIONS.GET_DOORS.fulfilled,
  payload: response,
});

export const getDoorsRejected = error => ({
  type: ACTIONS.GET_DOORS.rejected,
  payload: error,
  isError: true,
});

export const createDoor = params => ({
  type: ACTIONS.CREATE_DOOR.base,
  payload: params,
});

export const createDoorFulfilled = response => ({
  type: ACTIONS.CREATE_DOOR.fulfilled,
  payload: response,
});

export const createDoorRejected = error => ({
  type: ACTIONS.CREATE_DOOR.rejected,
  payload: error,
  isError: true,
});

export const getDoor = params => ({
  type: ACTIONS.GET_DOOR.base,
  payload: params,
});

export const getDoorFulfilled = response => ({
  type: ACTIONS.GET_DOOR.fulfilled,
  payload: response,
});

export const getDoorRejected = error => ({
  type: ACTIONS.GET_DOOR.rejected,
  payload: error,
  isError: true,
});

export const openDoor = params => ({
  type: ACTIONS.OPEN_DOOR.base,
  payload: params,
});

export const openDoorFulfilled = response => ({
  type: ACTIONS.OPEN_DOOR.fulfilled,
  payload: response,
});

export const openDoorRejected = error => ({
  type: ACTIONS.OPEN_DOOR.rejected,
  payload: error,
  isError: true,
});
