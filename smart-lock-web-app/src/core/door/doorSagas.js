import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../constants/actionTypes';
import {
  getDoorsFulfilled,
  getDoorsRejected,
  createDoorFulfilled,
  createDoorRejected,
  getDoorFulfilled,
  getDoorRejected,
  openDoorFulfilled,
  openDoorRejected,
} from './doorActions';
import doorApi from './doorApi';

function* getDoors() {
  try {
    const doors = yield call(doorApi.getDoors);
    yield put(getDoorsFulfilled(doors));
  } catch (e) {
    yield put(getDoorsRejected(e));
  }
}

function* createDoor(action) {
  try {
    const profile = yield call(doorApi.createDoor, action.payload);
    yield put(createDoorFulfilled(profile));
  } catch (e) {
    yield put(createDoorRejected(e));
  }
}

function* getDoor(action) {
  try {
    const doors = yield call(doorApi.getDoor, action.payload);
    yield put(getDoorFulfilled(doors));
  } catch (e) {
    yield put(getDoorRejected(e));
  }
}


function* openDoor(action) {
  try {
    const profile = yield call(doorApi.openDoor, action.payload);
    yield put(openDoorFulfilled(profile));
  } catch (e) {
    yield put(openDoorRejected(e));
  }
}

const doorSagas = [
  takeEvery(ACTIONS.GET_DOORS.base, getDoors),
  takeEvery(ACTIONS.CREATE_DOOR.base, createDoor),
  takeEvery(ACTIONS.GET_DOOR.base, getDoor),
  takeEvery(ACTIONS.OPEN_DOOR.base, openDoor),
];

export default doorSagas;
