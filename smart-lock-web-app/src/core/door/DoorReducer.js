import * as ACTIONS from '../../constants/actionTypes';

const defaultInitialState = {
  doors: [],
  door: {},
  doorApiErrors: {},
  hasActiveGetDoorsAction: false,
  hasActiveCreateDoorAction: false,
  hasActiveOpenDoorAction: false,
  candidateDoorIdToOpen: '',
};

const defaultMessage = 'Oops! Something is broken';

function DoorReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.GET_DOORS.base: {
        newState = {
          ...state,
          hasActiveGetDoorsAction: true,
        };
        break;
      }


      case ACTIONS.GET_DOORS.fulfilled: {
        const doors = action.payload.data;

        newState = {
          ...state,
          doors,
          hasActiveGetDoorsAction: false,
        };

        break;
      }

      case ACTIONS.GET_DOORS.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          doorApiErrors: error,
          hasActiveGetDoorsAction: false,
        };

        break;
      }

      case ACTIONS.CREATE_DOOR.base: {
        newState = {
          ...state,
          hasActiveCreateDoorAction: true,
        };
        break;
      }


      case ACTIONS.CREATE_DOOR.fulfilled: {
        const { door } = action.payload.data;
        const { doors } = state;


        newState = {
          ...state,
          doors: [
            ...doors,
            door,
          ],
          doorApiErrors: {},
          hasActiveCreateDoorAction: false,
        };

        break;
      }

      case ACTIONS.CREATE_DOOR.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          doorApiErrors: error,
          hasActiveCreateDoorAction: false,
        };

        break;
      }

      case ACTIONS.GET_DOOR.fulfilled: {
        const door = action.payload.data;

        newState = {
          ...state,
          door,
        };

        break;
      }

      case ACTIONS.GET_DOOR.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          doorApiErrors: error,
        };

        break;
      }

      case ACTIONS.OPEN_DOOR.base: {
        newState = {
          ...state,
          candidateDoorIdToOpen: action.payload.id,
          hasActiveOpenDoorAction: true,
        };
        break;
      }


      case ACTIONS.OPEN_DOOR.fulfilled: {
        newState = {
          ...state,
          doorApiErrors: {},
          hasActiveOpenDoorAction: false,
        };

        break;
      }

      case ACTIONS.OPEN_DOOR.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          doorApiErrors: error,
          hasActiveOpenDoorAction: false,
        };

        break;
      }


      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default DoorReducer;
