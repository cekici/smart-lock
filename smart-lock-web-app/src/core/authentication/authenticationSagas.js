import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../constants/actionTypes';
import {
  getProfileFulfilled,
  getProfileRejected,
  loginFulfilled,
  loginRejected,
  logoutFulfilled,
  logoutRejected,
} from './authenticationActions';
import authenticationApi from './authenticationApi';

function* login(action) {
  try {
    const profile = yield call(authenticationApi.login, action.payload);
    yield put(loginFulfilled(profile));
  } catch (e) {
    yield put(loginRejected(e));
  }
}

function* getProfile() {
  try {
    const profile = yield call(authenticationApi.getProfile);
    yield put(getProfileFulfilled(profile));
  } catch (e) {
    yield put(getProfileRejected(e));
  }
}

function* logout() {
  try {
    yield call(authenticationApi.logout);
    yield put(logoutFulfilled());
  } catch (e) {
    yield put(logoutRejected(e));
  }
}

const authenticationSagas = [
  takeEvery(ACTIONS.LOGIN.base, login),
  takeEvery(ACTIONS.GET_PROFILE.base, getProfile),
  takeEvery(ACTIONS.LOGOUT.base, logout),
];

export default authenticationSagas;
