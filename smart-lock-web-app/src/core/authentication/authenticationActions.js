import * as ACTIONS from '../../constants/actionTypes';

export const login = params => ({
  type: ACTIONS.LOGIN.base,
  payload: params,
});

export const loginFulfilled = response => ({
  type: ACTIONS.LOGIN.fulfilled,
  payload: response,
});

export const loginRejected = error => ({
  type: ACTIONS.LOGIN.rejected,
  payload: error,
  isError: true,
});

export const getProfile = () => ({
  type: ACTIONS.GET_PROFILE.base,
});

export const getProfileFulfilled = response => ({
  type: ACTIONS.GET_PROFILE.fulfilled,
  payload: response,
});

export const getProfileRejected = error => ({
  type: ACTIONS.GET_PROFILE.rejected,
  payload: error,
  isError: true,
});

export const logout = () => ({
  type: ACTIONS.LOGOUT.base,
});

export const logoutFulfilled = response => ({
  type: ACTIONS.LOGOUT.fulfilled,
  payload: response,
});

export const logoutRejected = error => ({
  type: ACTIONS.LOGOUT.rejected,
  payload: error,
  isError: true,
});
