import networkManager from '../../common/network/networkManager';

const authenticationApi = {
  login(params) {
    return networkManager.api.post('/login', params);
  },

  getProfile() {
    return networkManager.api.get('/me');
  },

  logout() {
    return networkManager.api.post('/logout');
  },
};

export default authenticationApi;
