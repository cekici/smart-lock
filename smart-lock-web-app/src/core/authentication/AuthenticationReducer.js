import authManager from '../../common/auth/authManager';
import * as ACTIONS from '../../constants/actionTypes';

const authToken = authManager.getToken();
const defaultInitialState = {
  authToken,
  profile: {},
  authenticationApiErrors: {},
  isSessionCheckFinished: !authToken,
  isTokenValid: false,
  hasActiveLoginAction: false,
};

const defaultMessage = 'Oops! Something is broken';

function AuthenticationReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.LOGIN.base: {
        newState = {
          ...state,
          hasActiveLoginAction: true,
        };
        break;
      }

      case ACTIONS.LOGIN.fulfilled: {
        const { user: profile } = action.payload.data;

        authManager.updateToken(profile.token);

        newState = {
          ...state,
          isTokenValid: true,
          profile,
          hasActiveLoginAction: false,
        };

        break;
      }

      case ACTIONS.LOGIN.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          authenticationApiErrors: error,
          hasActiveLoginAction: false,
        };

        break;
      }

      case ACTIONS.GET_PROFILE.fulfilled: {
        const { user: profile } = action.payload.data;

        newState = {
          ...state,
          isSessionCheckFinished: true,
          isTokenValid: true,
          profile,
        };

        break;
      }

      case ACTIONS.GET_PROFILE.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          isSessionCheckFinished: true,
          isTokenValid: false,
          authenticationApiErrors: error,
        };

        break;
      }

      case ACTIONS.LOGOUT.fulfilled: {
        authManager.updateToken(null);

        newState = {
          ...initialState,
          isSessionCheckFinished: true,
          isTokenValid: false,
        };

        break;
      }

      case ACTIONS.LOGOUT.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          authenticationApiErrors: error,
        };

        break;
      }

      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default AuthenticationReducer;
