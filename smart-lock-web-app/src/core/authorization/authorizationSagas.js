import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../constants/actionTypes';
import {
  getAuthorizationsFulfilled,
  getAuthorizationsRejected, updateAuthorizationFulfilled, updateAuthorizationRejected,
} from './authorizationActions';
import authorizationApi from './authorizationApi';

function* getAuthorizations(action) {
  try {
    const doors = yield call(authorizationApi.getAuthorizations, action.payload);
    yield put(getAuthorizationsFulfilled(doors));
  } catch (e) {
    yield put(getAuthorizationsRejected(e));
  }
}

function* updateAuthorization(action) {
  try {
    const authorization = yield call(authorizationApi.updateAuthorization, action.payload);
    yield put(updateAuthorizationFulfilled(authorization));
  } catch (e) {
    yield put(updateAuthorizationRejected(e));
  }
}

const authorizationSagas = [
  takeEvery(ACTIONS.GET_AUTHORIZATIONS.base, getAuthorizations),
  takeEvery(ACTIONS.UPDATE_AUTHORIZATION.base, updateAuthorization),
];

export default authorizationSagas;
