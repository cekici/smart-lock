import * as ACTIONS from '../../constants/actionTypes';

const defaultInitialState = {
  authorizations: [],
  authorizationApiErrors: {},
  hasActiveGetAuthorizationsAction: false,
  hasActiveUpdateAuthorizationAction: false,
  candidateUpdateAuthorizationId: '',
};

const defaultMessage = 'Oops! Something is broken';

function AuthorizationReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.GET_AUTHORIZATIONS.base: {
        newState = {
          ...state,
          hasActiveGetAuthorizationsAction: true,
        };
        break;
      }

      case ACTIONS.GET_AUTHORIZATIONS.fulfilled: {
        const authorizations = action.payload.data;

        newState = {
          ...state,
          authorizations,
          hasActiveGetAuthorizationsAction: false,
        };

        break;
      }

      case ACTIONS.GET_AUTHORIZATIONS.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          authorizationApiErrors: error,
          hasActiveGetAuthorizationsAction: false,
        };

        break;
      }

      case ACTIONS.UPDATE_AUTHORIZATION.base: {
        newState = {
          ...state,
          candidateUpdateAuthorizationId: action.payload.id,
          hasActiveUpdateAuthorizationAction: true,
        };
        break;
      }

      case ACTIONS.UPDATE_AUTHORIZATION.fulfilled: {
        const authorization = action.payload.data;
        const { authorizations, candidateUpdateAuthorizationId } = state;

        const authorizationIndex = authorizations.findIndex(item => item.id === candidateUpdateAuthorizationId);

        newState = {
          ...state,
          authorizations: [
            ...authorizations.slice(0, authorizationIndex),
            authorization,
            ...authorizations.slice(authorizationIndex + 1),
          ],
          authorizationApiErrors: {},
          hasActiveUpdateAuthorizationAction: false,
        };

        break;
      }

      case ACTIONS.UPDATE_AUTHORIZATION.rejected: {
        const error = {
          api: action.payload.response.data || { message: defaultMessage },
        };

        newState = {
          ...state,
          authorizationApiErrors: error,
          hasActiveUpdateAuthorizationAction: false,
        };

        break;
      }

      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default AuthorizationReducer;
