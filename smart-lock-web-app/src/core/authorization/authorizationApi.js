import networkManager from '../../common/network/networkManager';

const authorizationApi = {
  getAuthorizations(params) {
    let queryParamString = '';

    if (params.door) {
      queryParamString = `door=${params.door}`;
    } else if (params.user) {
      queryParamString = `user=${params.user}`;
    }

    return networkManager.api.get(`/authorizations?${queryParamString}`);
  },

  updateAuthorization(params) {
    return networkManager.api.put(`/authorization/${params.id}`, { status: params.status });
  },
};

export default authorizationApi;
