import * as ACTIONS from '../../constants/actionTypes';

export const getAuthorizations = params => ({
  type: ACTIONS.GET_AUTHORIZATIONS.base,
  payload: params,
});

export const getAuthorizationsFulfilled = response => ({
  type: ACTIONS.GET_AUTHORIZATIONS.fulfilled,
  payload: response,
});

export const getAuthorizationsRejected = error => ({
  type: ACTIONS.GET_AUTHORIZATIONS.rejected,
  payload: error,
  isError: true,
});

export const updateAuthorization = params => ({
  type: ACTIONS.UPDATE_AUTHORIZATION.base,
  payload: params,
});

export const updateAuthorizationFulfilled = response => ({
  type: ACTIONS.UPDATE_AUTHORIZATION.fulfilled,
  payload: response,
});

export const updateAuthorizationRejected = error => ({
  type: ACTIONS.UPDATE_AUTHORIZATION.rejected,
  payload: error,
  isError: true,
});
