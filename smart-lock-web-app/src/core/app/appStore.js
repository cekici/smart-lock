import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import authenticationSagas from '../authentication/authenticationSagas';
import AuthenticationReducer from '../authentication/AuthenticationReducer';
import logSagas from '../log/logSagas';
import LogReducer from '../log/LogReducer';
import userSagas from '../user/userSagas';
import UserReducer from '../user/UserReducer';
import doorSagas from '../door/doorSagas';
import DoorReducer from '../door/DoorReducer';
import authorizationSagas from '../authorization/authorizationSagas';
import AuthorizationReducer from '../authorization/AuthorizationReducer';

function* rootSaga() {
  yield all([
    ...authenticationSagas,
    ...logSagas,
    ...userSagas,
    ...doorSagas,
    ...authorizationSagas
  ]);
}

const reducers = {
  authenticationState: new AuthenticationReducer(),
  logState: new LogReducer(),
  userState: new UserReducer(),
  doorState: new DoorReducer(),
  authorizationState: new AuthorizationReducer()
};

const rootReducer = combineReducers(reducers);
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store;

