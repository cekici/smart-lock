const FULFILLED_KEYWORD = 'FULFILLED';
const REJECTED_KEYWORD = 'REJECTED';

function createActions(actionName) {
  return {
    base: actionName,
    get fulfilled() {
      return `${this.base}_${FULFILLED_KEYWORD}`;
    },
    get rejected() {
      return `${this.base}_${REJECTED_KEYWORD}`;
    },
  };
}

export const LOGIN = createActions('LOGIN');
export const GET_PROFILE = createActions('GET_PROFILE');
export const GET_USERS = createActions('GET_USERS');
export const CREATE_USER = createActions('CREATE_USER');
export const GET_USER = createActions('GET_USER');
export const GET_DOORS = createActions('GET_DOORS');
export const CREATE_DOOR = createActions('CREATE_DOOR');
export const GET_DOOR = createActions('GET_DOOR');
export const GET_AUTHORIZATIONS = createActions('GET_AUTHORIZATIONS');
export const UPDATE_AUTHORIZATION = createActions('UPDATE_AUTHORIZATION');
export const GET_LOGS = createActions('GET_LOGS');
export const OPEN_DOOR = createActions('OPEN_DOOR');
export const LOGOUT = createActions('LOGOUT');
