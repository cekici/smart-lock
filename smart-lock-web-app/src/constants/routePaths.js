export const ROUTE_DASHBOARD = '/dashboard';
export const ROUTE_LOGIN = '/login';
export const ROUTE_USERS = '/users';
export const ROUTE_USER = '/user/:id';
export const ROUTE_DOORS = '/doors';
export const ROUTE_DOOR = '/door/:id';
export const ROUTE_LOGS = '/logs';
