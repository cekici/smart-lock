import axios from 'axios';

const baseConfig = {
  baseURL: 'http://localhost:3000/api'
};

const networkManager = {
  init() {
    this.api = axios.create(baseConfig);
  },

  updateAuthToken(authToken) {
    const apiConfig = {
      ...baseConfig,
    };

    if (authToken) {
      const headers = apiConfig.headers || {};
      const commonHeaders = headers.common || {};

      apiConfig.headers = {
        ...headers,
        common: {
          ...commonHeaders,
          'X-Access-Token': authToken
        },
      };
    }

    this.api = axios.create(apiConfig);
  }
};

networkManager.init();
export default networkManager;
