/* global localStorage */

import networkManager from '../network/networkManager';


const authManager = {
  init() {
    const authToken = localStorage.getItem('authToken');

    if (authToken) {
      networkManager.updateAuthToken(authToken);
    }

    this.token = authToken;
  },

  getToken() {
    return this.token;
  },

  updateToken(authToken) {
    this.token = authToken;
    localStorage.setItem('authToken', authToken);
    networkManager.updateAuthToken(authToken);
  },
};

authManager.init();
export default authManager;
