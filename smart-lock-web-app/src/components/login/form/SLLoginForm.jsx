import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLButton from '../../button/SLButton';
import SLInputField from '../../input/SLInputField';

import './_sl-login-form.scss';

export default class SLLoginForm extends Component {
  static propTypes = {
    errorMap: PropTypes.object.isRequired,
    formInitialValueMap: PropTypes.object.isRequired,
    hasActiveLoginAction: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      shouldDisplayErrors: false,
      valueMap: {
        ...props.formInitialValueMap,
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMap !== this.props.errorMap) {
      this.setState({
        shouldDisplayErrors: true,
      });
    }
  }

  handleUsernameChange = (event) => {
    const { valueMap } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      valueMap: {
        ...valueMap,
        username: event.target.value,
      },
    });
  }

  handlePasswordChange = (event) => {
    const { valueMap } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      valueMap: {
        ...valueMap,
        password: event.target.value,
      },
    });
  }

  handleSubmit = (event) => {
    const { onSubmit } = this.props;
    const { valueMap } = this.state;
    const validation = {};

    event.preventDefault();

    if (!valueMap.username) {
      validation.username = {
        message: "Username can't be empty",
      };
    }

    if (!valueMap.password) {
      validation.password = {
        message: "Password can't be empty",
      };
    }

    onSubmit(valueMap, validation);
  }

  render() {
    const { errorMap, hasActiveLoginAction } = this.props;
    const { valueMap, shouldDisplayErrors } = this.state;

    return (
      <form
        className="sl-login-form"
        onSubmit={this.handleSubmit}
      >

        <h6 className="sl-login-form-title">
          {'Welcome to TheCloudLock. Please login to use the application'}
        </h6>

        <div className="sl-login-field-list-container">
          <SLInputField
            type="input"
            value={valueMap.username}
            name="username"
            placeholder="Username"
            error={shouldDisplayErrors && errorMap.username}
            onChange={this.handleUsernameChange}
          />

          <SLInputField
            type="password"
            value={valueMap.password}
            name="password"
            placeholder="Password"
            error={shouldDisplayErrors && errorMap.password}
            onChange={this.handlePasswordChange}
          />
        </div>

        {errorMap.api &&
        <p className="sl-login-form-api-error-text">{errorMap.api.message}</p>
        }

        <SLButton
          type="submit"
          className="sl-login-form-submit-button"
          isDisabled={hasActiveLoginAction}
          isLoading={hasActiveLoginAction}
          text="Login"
        />
      </form>);
  }
}
