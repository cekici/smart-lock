import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLButton from '../../button/SLButton';
import SLInputField from '../../input/SLInputField';

import './_sl-user-form.scss';

export default class SLUserForm extends Component {
  static propTypes = {
    errorMap: PropTypes.object.isRequired,
    formInitialValueMap: PropTypes.object.isRequired,
    hasActiveCreateAction: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      shouldDisplayErrors: false,
      valueMap: {
        ...props.formInitialValueMap,
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMap !== this.props.errorMap) {
      this.setState({
        shouldDisplayErrors: true,
      });
    }
  }

  handleFirstNameChange = (event) => {
    const { valueMap } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      valueMap: {
        ...valueMap,
        firstName: event.target.value,
      },
    });
  }

  handleLastNameChange = (event) => {
    const { valueMap } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      valueMap: {
        ...valueMap,
        lastName: event.target.value,
      },
    });
  }

  handleUsernameChange = (event) => {
    const { valueMap } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      valueMap: {
        ...valueMap,
        username: event.target.value,
      },
    });
  }


  handleSubmit = (event) => {
    const { onSubmit } = this.props;
    const { valueMap } = this.state;
    const validation = {};

    event.preventDefault();

    if (!valueMap.firstName) {
      validation.firstName = {
        message: "First name can't be empty",
      };
    }

    if (!valueMap.lastName) {
      validation.lastName = {
        message: "Last name can't be empty",
      };
    }

    if (!valueMap.username) {
      validation.username = {
        message: "Username can't be empty",
      };
    }

    onSubmit(valueMap, validation);
  }

  render() {
    const { errorMap, hasActiveCreateAction } = this.props;
    const { valueMap, shouldDisplayErrors } = this.state;

    return (
      <form
        className="sl-user-form"
        onSubmit={this.handleSubmit}
      >

        <h6 className="sl-user-form-title">
          {'Create a new user'}
        </h6>

        <div className="sl-user-field-list-container">
          <SLInputField
            type="input"
            value={valueMap.firstName}
            name="firstname"
            placeholder="First Name"
            error={shouldDisplayErrors && errorMap.firstName}
            onChange={this.handleFirstNameChange}
          />

          <SLInputField
            type="input"
            value={valueMap.lastName}
            name="lastname"
            placeholder="Last Name"
            error={shouldDisplayErrors && errorMap.lastName}
            onChange={this.handleLastNameChange}
          />

          <SLInputField
            type="input"
            value={valueMap.username}
            name="username"
            placeholder="Username"
            error={shouldDisplayErrors && errorMap.username}
            onChange={this.handleUsernameChange}
          />

        </div>

        <SLButton
          type="submit"
          className="sl-user-form-submit-button"
          isDisabled={hasActiveCreateAction}
          isLoading={hasActiveCreateAction}
          text="Create User"
        />
      </form>);
  }
}
