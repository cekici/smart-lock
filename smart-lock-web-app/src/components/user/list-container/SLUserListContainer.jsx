import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLUserItem from '../list-item/SLUserItem';

import './_sl-user-list-container.scss';

export default class SLUserListContainer extends Component {
  static propTypes = {
    isFetchFinished: PropTypes.bool.isRequired,
    users: PropTypes.array.isRequired,
    onClick: PropTypes.func.isRequired,
  }

  render() {
    const {
      isFetchFinished,
      users,
      onClick,
    } = this.props;

    return (
      <div className="sl-user-list-container">
        {isFetchFinished &&
        <div className="sl-user-list-content">
          <div className="sl-user-list-header">
            <span className="sl-user-list-header-name">Name</span>
            <span className="sl-user-list-header-username">Username</span>
            <span className="sl-user-list-header-role">Role</span>
          </div>

          {users.length ?
            <ul className="sl-user-list">
              {users.map(user =>
                (<SLUserItem
                  user={user}
                  key={user.id}
                  onClick={onClick}
                />
                ))}
            </ul>
            :
            <p className="sl-user-list-empty-text">No users yet</p>
          }
        </div>
        }
      </div>
    );
  }
}
