import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './_sl-user-item.scss';

export default class SLUserItem extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.user.id !== this.props.user.id;
  }

  handleClick = () => {
    const { user, onClick } = this.props;
    onClick(user.id);
  }

  render() {
    const { user } = this.props;

    const {
      username,
      firstName,
      lastName,
      role,
    } = user;

    const userName = `${firstName} ${lastName}`;

    const roleClassName = classNames('sl-user-item-role', {
      'is-admin': role === 'admin',
    });

    return (
      <li
        className="sl-user-item"
        onClick={this.handleClick}
      >
        <span className="sl-user-item-name">{userName}</span>
        <span className="sl-user-item-username">{username}</span>
        <span className={roleClassName}>{role}</span>
      </li>
    );
  }
}
