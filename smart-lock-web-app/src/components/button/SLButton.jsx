import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './_sl-button.scss';

export default class SLButton extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    text: PropTypes.string,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    type: 'button',
    isLoading: false,
  }

  handleClick = (event) => {
    const { onClick } = this.props;

    if (onClick) {
      onClick(event);
    }
  }

  render() {
    const {
      className, text, isDisabled, isLoading, type,
    } = this.props;

    return (
      <button
        className={classNames('sl-button', className)}
        disabled={isDisabled}
        type={type}
        onClick={this.handleClick}
      >
        <div className="sl-button-content">
          {isLoading ?
            <svg
              className="sl-button-loader"
              width="20"
              height="20"
              viewBox="0 0 52 52"
              xmlns="http://www.w3.org/2000/svg"
            >

              <circle
                className="sl-button-loader-path"
                cx="26"
                cy="26"
                r="20"
                fill="none"
                strokeWidth="6"
              />
            </svg> :
            <span className="sl-button-text">
              {text}
            </span>
          }
        </div>
      </button>);
  }
}
