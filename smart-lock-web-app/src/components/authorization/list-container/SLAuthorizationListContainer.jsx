import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLAuthorizationItem from '../list-item/SLAuthorizationItem';

import './_sl-authorization-list-container.scss';

export default class SLAuthorizationListContainer extends Component {
  static propTypes = {
    authorizations: PropTypes.array.isRequired,
    user: PropTypes.object,
    users: PropTypes.array,
    door: PropTypes.object,
    doors: PropTypes.array,
    candidateUpdateAuthorizationId: PropTypes.string.isRequired,
    hasActiveUpdateAuthorizationAction: PropTypes.bool.isRequired,
    onChangeAuthorizationClick: PropTypes.func.isRequired,
  }

  render() {
    const {
      authorizations,
      user,
      users,
      door,
      doors,
      candidateUpdateAuthorizationId,
      hasActiveUpdateAuthorizationAction,
      onChangeAuthorizationClick,
    } = this.props;

    const isUsers = door && door.id;

    return (
      <div className="sl-authorization-list-container">
        <div className="sl-authorization-list-content">
          <div className="sl-authorization-list-header">
            <span className="sl-authorization-list-header-name">{
              isUsers ?
                'Authorization Status of Users' :
                'Access Status to Doors'
            }
            </span>
          </div>

          <ul className="sl-authorization-list">
            {authorizations.map(authorization =>
              (<SLAuthorizationItem
                authorization={authorization}
                user={user}
                users={users}
                door={door}
                doors={doors}
                key={authorization.id}
                candidateUpdateAuthorizationId={candidateUpdateAuthorizationId}
                hasActiveUpdateAuthorizationAction={hasActiveUpdateAuthorizationAction}
                onChangeAuthorizationClick={onChangeAuthorizationClick}
              />
              ))}
          </ul>

        </div>
      </div>
    );
  }
}
