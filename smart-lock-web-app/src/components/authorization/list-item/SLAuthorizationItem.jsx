import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import SLButton from '../../button/SLButton';

import './_sl-authorization-item.scss';

export default class SLAuthorizationItem extends Component {
  static propTypes = {
    authorization: PropTypes.object.isRequired,
    user: PropTypes.object,
    users: PropTypes.array,
    door: PropTypes.object,
    doors: PropTypes.array,
    candidateUpdateAuthorizationId: PropTypes.string.isRequired,
    hasActiveUpdateAuthorizationAction: PropTypes.bool.isRequired,
    onChangeAuthorizationClick: PropTypes.func.isRequired,
  }

  handleClick = () => {
    const { authorization, onChangeAuthorizationClick } = this.props;
    onChangeAuthorizationClick(authorization.id, !authorization.status);
  }

  render() {
    const {
      authorization,
      user,
      users,
      door,
      doors,
      candidateUpdateAuthorizationId,
      hasActiveUpdateAuthorizationAction,
    } = this.props;

    const {
      id,
      status,
      doorId,
      userId,
    } = authorization;

    let displayText;

    if (user && user.id) {
      const authorizationDoor = doors.find(item => item.id === doorId);
      displayText = authorizationDoor.name;
    } else if (door && door.id) {
      const authorizationUser = users.find(item => item.id === userId);
      displayText = `${authorizationUser.firstName} ${authorizationUser.lastName}`;
    }

    const statusClassName = classNames('sl-authorization-item-status', {
      'is-allowed': status,
      'is-disallowed': !status,
    });

    return (
      <li className="sl-authorization-item">
        <span className="sl-authorization-item-name">{displayText}</span>
        <span className={statusClassName}>{status ? 'Access Allowed' : 'Access Disallowed'}</span>
        <SLButton
          type="button"
          className="sl-authorization-item-change-status-button"
          isDisabled={hasActiveUpdateAuthorizationAction}
          isLoading={hasActiveUpdateAuthorizationAction && id === candidateUpdateAuthorizationId}
          text={status ? 'Revoke' : 'Allow'}
          onClick={this.handleClick}
        />
      </li>
    );
  }
}
