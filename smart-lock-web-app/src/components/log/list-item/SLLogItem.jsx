import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';

import './_sl-log-item.scss';

export default class SLLogItem extends Component {
  static propTypes = {
    log: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    doors: PropTypes.array.isRequired,
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.log.id !== this.props.log.id;
  }

  render() {
    const {
      log,
      users,
      doors,
    } = this.props;

    const {
      userId,
      doorId,
      status,
      time,
    } = log;

    const user = users.find(item => item.id === userId);
    const door = doors.find(item => item.id === doorId);
    const formattedTime = moment(time).format('MMM D, YYYY H:mm a');

    const userName = `${user.firstName} ${user.lastName}`;
    const doorName = door.name;

    const statusClassName = classNames('sl-log-item-status', {
      'is-success': status,
      'is-error': !status,
    });

    const statusText = status ? 'Access Granted' : 'Access Denied';


    return (
      <li className="sl-log-item">
        <span className="sl-log-item-user">{userName}</span>
        <span className="sl-log-item-door">{doorName}</span>
        <span className={statusClassName}>{statusText}</span>
        <span className="sl-log-item-time">{formattedTime}</span>
      </li>
    );
  }
}
