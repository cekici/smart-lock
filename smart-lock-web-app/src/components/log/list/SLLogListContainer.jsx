import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLLogItem from '../list-item/SLLogItem';

import './_sl-log-list-container.scss';

export default class SLLogListContainer extends Component {
  static propTypes = {
    isFetchFinished: PropTypes.bool.isRequired,
    logs: PropTypes.array.isRequired,
    users: PropTypes.array.isRequired,
    doors: PropTypes.array.isRequired,
  }

  render() {
    const {
      isFetchFinished,
      logs,
      users,
      doors,
    } = this.props;

    return (
      <div className="sl-log-list-container">
        {isFetchFinished &&
        <div className="sl-log-list-content">
          <div className="sl-log-list-header">
            <span className="sl-log-list-header-user">Name</span>
            <span className="sl-log-list-header-door">Door</span>
            <span className="sl-log-list-header-status">Status</span>
            <span className="sl-log-list-header-time">Time</span>
          </div>
          {logs.length ?
            <ul className="sl-log-list">
              {logs.map(log =>
                (<SLLogItem
                  log={log}
                  users={users}
                  doors={doors}
                  key={log.id}
                />
                ))}
            </ul>
            :
            <p className="sl-log-list-empty-text">No logs yet</p>
          }
        </div>
        }
      </div>
    );
  }
}
