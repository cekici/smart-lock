/* globals window */

import React, { Component } from 'react';
import { withRouter, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import classNames from 'classnames';

import * as ROUTE_PATHS from '../../constants/routePaths';
import * as authenticationActions from '../../core/authentication/authenticationActions';
import SLButton from '../button/SLButton';

import './_sl-menu.scss';

const MENU_BREAKPOINT = 720;

class SmartSLMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };

    this.handleMenuVisibility = this.handleMenuVisibility.bind(this);
  }

  componentDidMount() {
    this.handleMenuVisibility();
    window.addEventListener('resize', this.handleMenuVisibility);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isVisible: nextProps.isVisible,
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleMenuVisibility);
  }

  handleMenuVisibility() {
    this.setState({
      isVisible: window.width <= MENU_BREAKPOINT,
    });
  }

  handleToggleMenuButtonClick = () => {
    this.setState({
      isVisible: true,
    });
  }

  handleMenuClose = () => {
    this.setState({
      isVisible: false,
    });
  }


  handleLogoutClick = () => {
    const { dispatch } = this.props;
    dispatch(authenticationActions.logout());
  }

  render() {
    const { profile } = this.props;
    const { isVisible } = this.state;

    const containerClassName = classNames('sl-menu', {
      'is-visible': isVisible,
    });

    const isAdmin = profile.id && profile.role === 'admin';

    return (
      <aside className={containerClassName}>
        <SLButton
          type="button"
          text="Menu"
          className="sl-button sl-menu-toggle-button"
          onClick={this.handleToggleMenuButtonClick}
        />

        <div className="sl-menu-content-wrapper">
          <div className="sl-menu-content">
            <nav className="sl-menu-navigation">
              <ul className="sl-menu-navigation-list">
                <li className="sl-menu-navigation-item">
                  <NavLink
                    to={ROUTE_PATHS.ROUTE_DASHBOARD}
                    className="sl-menu-navigation-link"
                    activeClassName="sl-menu-navigation-link--active"
                    onClick={this.handleMenuClose}
                  >
                    <span className="sl-menu-navigation-text">Dashboard</span>
                  </NavLink>
                </li>
                {isAdmin &&
                <li className="sl-menu-navigation-item">
                  <NavLink
                    to={ROUTE_PATHS.ROUTE_USERS}
                    className="sl-menu-navigation-link"
                    activeClassName="sl-menu-navigation-link--active"
                    onClick={this.handleMenuClose}
                  >
                    <span className="sl-menu-navigation-text">Users</span>
                  </NavLink>
                </li>
                }
                {isAdmin &&
                <li className="sl-menu-navigation-item">
                  <NavLink
                    to={ROUTE_PATHS.ROUTE_DOORS}
                    className="sl-menu-navigation-link"
                    activeClassName="sl-menu-navigation-link--active"
                    onClick={this.handleMenuClose}
                  >
                    <span className="sl-menu-navigation-text">Doors</span>
                  </NavLink>
                </li>
                }
                {isAdmin &&
                <li className="sl-menu-navigation-item">
                  <NavLink
                    to={ROUTE_PATHS.ROUTE_LOGS}
                    className="sl-menu-navigation-link"
                    activeClassName="sl-menu-navigation-link--active"
                    onClick={this.handleMenuClose}
                  >
                    <span className="sl-menu-navigation-text">Logs</span>
                  </NavLink>
                </li>
                }
              </ul>
            </nav>

            <SLButton
              type="button"
              onClick={this.handleLogoutClick}
              className="sl-menu-logout-button"
              text="Logout"
            />
          </div>
        </div>

        <div
          className="sl-menu-overlay"
          onClick={this.handleMenuClose}
        />
      </aside>
    );
  }
}

function mapStateToProps(state) {
  const {
    authenticationState: {
      profile,
    },
  } = state;

  return {
    profile,
  };
}

const ConnectedSLMenu = connect(mapStateToProps)(SmartSLMenu);
const ConnectedSLMenuWithRouter = withRouter(ConnectedSLMenu);
export default ConnectedSLMenuWithRouter;
