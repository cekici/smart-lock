import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './_sl-input-field.scss';

export default class SLInput extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    minLength: PropTypes.number,
    maxLength: PropTypes.number,
    error: PropTypes.any,
  }

  static defaultProps = {
    type: 'text',
  }

  render() {
    const {
      type,
      value,
      name,
      placeholder,
      onChange,
      className,
      minLength,
      maxLength,
      error,
    } = this.props;

    const fieldClassName = classNames('sl-input-field', className, {
      'has-error': error,
    });

    const inputClassName = classNames('sl-input', {
      'has-error': error,
    });

    return (
      <div className={fieldClassName}>
        <input
          type={type}
          value={value}
          name={name}
          placeholder={placeholder}
          onChange={onChange}
          className={inputClassName}
          minLength={minLength}
          maxLength={maxLength}
        />

        {error &&
        <p className="sl-input-field--error">
          {error.message}
        </p>
        }
      </div>

    );
  }
}
