import React, { Component } from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';

import SLButton from '../../button/SLButton';

import './_sl-door-button.scss';

ReactModal.setAppElement('#app');

export default class SLDoorButton extends Component {
  static propTypes = {
    door: PropTypes.object.isRequired,
    hasActiveOpenDoorAction: PropTypes.bool.isRequired,
    candidateDoorIdToOpen: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.door.id !== this.props.door.id ||
      nextProps.hasActiveOpenDoorAction !== this.props.hasActiveOpenDoorAction;
  }

  handleClick = () => {
    const { door, onClick } = this.props;
    onClick(door.id);
  }

  render() {
    const {
      door,
      hasActiveOpenDoorAction,
      candidateDoorIdToOpen,
    } = this.props;

    const {
      id,
      name,
    } = door;


    return (
      <div className="sl-door-button-container">
        <SLButton
          type="button"
          className="sl-door-button"
          isDisabled={hasActiveOpenDoorAction}
          isLoading={hasActiveOpenDoorAction && candidateDoorIdToOpen === id}
          text={name}
          onClick={this.handleClick}
        />
      </div>
    );
  }
}
