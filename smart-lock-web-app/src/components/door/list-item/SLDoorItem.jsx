import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './_sl-door-item.scss';

export default class SLDoorItem extends Component {
  static propTypes = {
    door: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.door.id !== this.props.door.id;
  }

  handleClick = () => {
    const { door, onClick } = this.props;
    onClick(door.id);
  }

  render() {
    const { door } = this.props;

    const {
      name,
    } = door;


    return (
      <li
        className="sl-door-item"
        onClick={this.handleClick}
      >
        <span className="sl-door-item-name">{name}</span>
      </li>
    );
  }
}
