import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLDoorItem from '../list-item/SLDoorItem';

import './_sl-door-list-container.scss';

export default class SLDoorListContainer extends Component {
  static propTypes = {
    isFetchFinished: PropTypes.bool.isRequired,
    doors: PropTypes.array.isRequired,
    onClick: PropTypes.func.isRequired,
  }

  render() {
    const {
      isFetchFinished,
      doors,
      onClick,
    } = this.props;

    return (
      <div className="sl-door-list-container">
        {isFetchFinished &&
        <div className="sl-door-list-content">
          <div className="sl-door-list-header">
            <span className="sl-door-list-header-name">Name</span>
          </div>

          {doors.length ?
            <ul className="sl-door-list">
              {doors.map(door =>
                (<SLDoorItem
                  door={door}
                  key={door.id}
                  onClick={onClick}
                />
                ))}
            </ul>
            :
            <p className="sl-door-list-empty-text">No doors yet</p>
          }
        </div>
        }
      </div>
    );
  }
}
