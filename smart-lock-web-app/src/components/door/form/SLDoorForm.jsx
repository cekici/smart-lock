import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SLButton from '../../button/SLButton';
import SLInputField from '../../input/SLInputField';

import './_sl-door-form.scss';

export default class SLDoorForm extends Component {
  static propTypes = {
    errorMap: PropTypes.object.isRequired,
    formInitialValueMap: PropTypes.object.isRequired,
    hasActiveCreateAction: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      shouldDisplayErrors: false,
      valueMap: {
        ...props.formInitialValueMap,
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMap !== this.props.errorMap) {
      this.setState({
        shouldDisplayErrors: true,
      });
    }
  }

  handleNameChange = (event) => {
    const { valueMap } = this.state;

    this.setState({
      shouldDisplayErrors: false,
      valueMap: {
        ...valueMap,
        name: event.target.value,
      },
    });
  }

  handleSubmit = (event) => {
    const { onSubmit } = this.props;
    const { valueMap } = this.state;
    const validation = {};

    event.preventDefault();

    if (!valueMap.name) {
      validation.name = {
        message: "Name can't be empty",
      };
    }

    onSubmit(valueMap, validation);
  }

  render() {
    const { errorMap, hasActiveCreateAction } = this.props;
    const { valueMap, shouldDisplayErrors } = this.state;

    return (
      <form
        className="sl-door-form"
        onSubmit={this.handleSubmit}
      >

        <h6 className="sl-door-form-title">
          {'Create a new door'}
        </h6>

        <div className="sl-door-field-list-container">
          <SLInputField
            type="input"
            value={valueMap.name}
            name="name"
            placeholder="Name"
            error={shouldDisplayErrors && errorMap.name}
            onChange={this.handleNameChange}
          />
        </div>

        <SLButton
          type="submit"
          className="sl-door-form-submit-button"
          isDisabled={hasActiveCreateAction}
          isLoading={hasActiveCreateAction}
          text="Create Door"
        />
      </form>);
  }
}
