/* eslint-disable */
/* react/no-multi-comp*/

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';

import * as ROUTE_PATHS from '../constants/routePaths';
import * as authenticationActions from '../core/authentication/authenticationActions';
import ConnectedSLMenuWithRouter from '../components/menu/SLMenu';
import ConnectedSmartSLDashboardPageContentWithRouter from '../pages/dashboard/SLDashboardPageContent';
import ConnectedSmartSLDoorsPageContentWithRouter from '../pages/doors/SLDoorsPageContent';
import ConnectedSmartSLDoorPageContentWithRouter from '../pages/door/SLDoorPageContent';
import ConnectedSmartSLUsersPageContentWithRouter from '../pages/users/SLUsersPageContent';
import ConnectedSmartSLUserPageContentWithRouter from '../pages/user/SLUserPageContent';
import ConnectedSmartSLLogsPageContentWithRouter from '../pages/logs/SLLogsPageContent';
import ConnectedSmartSLLoginPageContentWithRouter from '../pages/login/SLLoginPageContent';

import './_sl-app.scss';

ReactModal.setAppElement('#app');

class SmartSLApp extends Component {
  componentWillMount() {
    const { dispatch, authToken } = this.props;

    if (authToken) {
      dispatch(authenticationActions.getProfile());
    }
  }

  componentDidUpdate(prevProps) {
    const { history, isTokenValid } = this.props;

    if (!isTokenValid && prevProps.isTokenValid) {
      history.replace('/login');
    }
  }

  renderMenuRoute = () => (
    <Switch>
      <Route
        path={ROUTE_PATHS.ROUTE_DASHBOARD}
        component={ConnectedSLMenuWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_DOORS}
        component={ConnectedSLMenuWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_DOOR}
        component={ConnectedSLMenuWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_USERS}
        component={ConnectedSLMenuWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_USER}
        component={ConnectedSLMenuWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_LOGS}
        component={ConnectedSLMenuWithRouter}
      />
    </Switch>
  )

  renderAuthenticatedPageRoute = () => (
    <Switch>
      <Route
        path={ROUTE_PATHS.ROUTE_DASHBOARD}
        component={ConnectedSmartSLDashboardPageContentWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_DOORS}
        component={ConnectedSmartSLDoorsPageContentWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_DOOR}
        component={ConnectedSmartSLDoorPageContentWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_USERS}
        component={ConnectedSmartSLUsersPageContentWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_USER}
        component={ConnectedSmartSLUserPageContentWithRouter}
      />

      <Route
        path={ROUTE_PATHS.ROUTE_LOGS}
        component={ConnectedSmartSLLogsPageContentWithRouter}
      />

      <Redirect
        from="/*"
        to="/dashboard"
      />
    </Switch>
  )

  renderGuestPageRoute = () => (
    <Switch>
      <Route
        path={ROUTE_PATHS.ROUTE_LOGIN}
        component={ConnectedSmartSLLoginPageContentWithRouter}
      />

      <Redirect
        from="/*"
        to="/login"
      />
    </Switch>
  )

  renderPageRoute = () => {
    const {
      isSessionCheckFinished,
      isTokenValid,
    } = this.props;

    if (isSessionCheckFinished) {
      return isTokenValid ?
        this.renderAuthenticatedPageRoute() :
        this.renderGuestPageRoute();
    }

    return null;
  }

  render() {
    const {
      isSessionCheckFinished,
      isTokenValid,
    } = this.props;

    return (
      <div className="sl-app-container">
        {isSessionCheckFinished && isTokenValid ?
          <div className="sl-app-menu">
            {this.renderMenuRoute()}
          </div> : null
        }

        {this.renderPageRoute()}
      </div>
    );
  }
}

const mapStateToProps = (state = {}) => {
  const {
    authenticationState: {
      authToken,
      isSessionCheckFinished,
      isTokenValid,
    },
  } = state;

  return {
    authToken,
    isSessionCheckFinished,
    isTokenValid,
  };
};

const ConnectedSmartSLApp = connect(mapStateToProps)(SmartSLApp);
const ConnectedSmartSLAppWithRouter = withRouter(ConnectedSmartSLApp);

class RootConnectedSmartSLApp extends Component {
  render() {
    return (
      <Router basename="/">
        <Route component={ConnectedSmartSLAppWithRouter} />
      </Router>
    );
  }
}

/* eslint-enable */

export default RootConnectedSmartSLApp;
