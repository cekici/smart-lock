var express = require('express');
var router = express.Router();

var auth = require('./auth.js');
var users = require('./users.js');
var doors = require('./doors.js');
var authorizations = require('./authorizations.js');
var logs = require('./logs.js');

/*
 * Routes that can be accessed by any one
 */
router.post('/api/login', auth.login);
router.get('/api/me', auth.getProfile);
/*
 * Routes that can be accessed only by authenticated users
 */
router.get('/api/doors', doors.getAll);
router.get('/api/door/:id', doors.getOne);
router.post('/api/door/:id/open', doors.open);

router.post('/api/logout', auth.logout);

/*
 * Routes that can be accessed only by authenticated & authorized users
 */
router.post('/api/door', doors.create);

router.get('/api/users', users.getAll);
router.get('/api/user/:id', users.getOne);
router.post('/api/user', users.create);

router.get('/api/authorizations', authorizations.getAll);
router.put('/api/authorization/:id', authorizations.update);

router.get('/api/logs', logs.getAll);

module.exports = router;