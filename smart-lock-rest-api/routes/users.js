var shortid = require('shortid');
var authorizations = require('./authorizations.js');
var dbModule = require('../db');
var db = dbModule.get();

var users = {
  getAll: function (req, res) {
    var allUsers = db.get('users').value();
    res.json(allUsers);
  },

  getOne: function (req, res) {
    var id = req.params.id;
    var user = db.get('users')
      .find({id: id})
      .value();

    res.json(user);
  },

  create: function (req, res) {
    var newUser = req.body;
    var id = shortid.generate();
    var username = newUser.username;
    var firstName = newUser.firstName;
    var lastName = newUser.lastName;
    var role = "staff";

    var user = {
      id: id,
      username: username,
      password: "123",
      firstName: firstName,
      lastName: lastName,
      role: role
    };

    db.get('users')
      .push(user)
      .write();

    var doors = db.get('doors').value();

    doors.forEach(function (door) {
      authorizations.create(id, door.id);
    });

    res.json(users.serializeUser(user));
  },

  serializeUser: function (rawUser, token) {
    var user = {
      id: rawUser.id,
      username: rawUser.username,
      firstName: rawUser.firstName,
      lastName: rawUser.lastName,
      role: rawUser.role
    };

    if(token) {
      user.token = token;
    }

    return {
      user: user
    };
  }
};


module.exports = users;