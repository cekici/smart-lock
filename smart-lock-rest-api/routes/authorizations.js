var shortid = require('shortid');
var dbModule = require('../db');
var db = dbModule.get();

var authorizations = {
  getAll: function (req, res) {
    var userId, doorId, authorizations;

    if (req.query) {
      userId = req.query.user;
      doorId = req.query.door;
    }

    authorizations = db.get('authorizations');

    if (userId) {
      authorizations = authorizations.filter({userId: userId});
    }

    if (doorId) {
      authorizations = authorizations.filter({doorId: doorId});
    }

    res.json(authorizations.value());
  },


  create: function (userId, doorId) {
    var user = db.get('users').find({id: userId}).value();
    var status = user.role === "admin";

    db
      .get('authorizations')
      .push({
        id: shortid.generate(),
        userId: userId,
        doorId: doorId,
        status: status
      })
      .write();
  },

  update: function (req, res) {
    var updateAuthorization = req.body;
    var id = req.params.id;
    var status = updateAuthorization.status;

    var authorization = db
      .get('authorizations')
      .find({id: id});

    authorization
      .assign({status: status})
      .write();

    res.json(authorization);
  },
};


module.exports = authorizations;