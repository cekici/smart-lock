var dbModule = require('../db');
var db = dbModule.get();

var logs = {
  getAll: function (req, res) {
    var allLogs = db.get('logs').value();
    res.json(allLogs);
  }
};


module.exports = logs;