var shortid = require('shortid');
var validateUser = require('../routes/auth').validateUser;
var authorizations = require('./authorizations.js');
var dbModule = require('../db');
var db = dbModule.get();

var doors = {
  open: function (req, res) {
    var door = db.get('doors').find({id: req.params.id}).value();
    var user = validateUser(req.headers['x-access-token']);
    var doorId = door.id;
    var userId = user.id;

    var authorization =
      db
        .get('authorizations')
        .find({doorId: doorId, userId: userId})
        .value();

    var status = authorization.status;

    db
      .get('logs')
      .push({
        id: shortid.generate(),
        userId: user.id,
        doorId: door.id,
        status: status,
        time: (new Date()).getTime()
      })
      .write();

    if (status) {
      res.status(200);
      res.json({
        status: status
      });
    } else {
      res.status(403);
      res.json({"message": "Not Authorized"});
    }
  },

  getAll: function (req, res) {
    var allDoors = db.get('doors').value();
    res.json(allDoors);
  },

  getOne: function (req, res) {
    var id = req.params.id;
    var door = db.get('doors')
      .find({id: id})
      .value()

    res.json(door);
  },

  create: function (req, res) {
    var newDoor = req.body;
    var id = shortid.generate();
    var name = newDoor.name;
    var door = {
      id: id,
      name: name
    };

    db.get('doors')
      .push(door)
      .write();

    var users = db.get('users').value();

    users.forEach(function(user) {
      authorizations.create(user.id, id);

    });

    res.json(doors.serializeDoor(door));

  },

  serializeDoor: function (door) {
    return {
      door: {
        id: door.id,
        name: door.name
      }
    };
  }
};


module.exports = doors;