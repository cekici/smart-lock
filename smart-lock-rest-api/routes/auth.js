var jwt = require("jwt-simple");
var getSecretKey = require("../config/secret.js");
var serializeUser = require("../routes/users").serializeUser;
var dbModule = require("../db");
var db = dbModule.get();

var auth = {

  login: function(req, res) {
    var username = req.body.username || "";
    var password = req.body.password || "";

    if (username === "" || password === "") {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
      return;
    }

    var userObj = auth.validate(username, password);

    if (!userObj) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
      return;
    }

    if (userObj) {
      var token = genToken(userObj);

      db.get("users")
        .find({username: username})
        .set({token: token})
        .write();

      res.json(serializeUser(userObj, token));
    }
  },

  getProfile: function(req, res) {
    try {
      var token = req.headers["x-access-token"];
      var obj = jwt.decode(token, getSecretKey());

      if(obj) {
        var username = obj.username;

        var user = db.get("users")
          .find({username: username})
          .value();

        res.json(serializeUser(user));
      } else {
        res.status(401);
        res.json({
          "status": 401,
          "message": "Invalid credentials"
        });
      }
    } catch(err) {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
    }
  },
  
  logout: function(req, res) {
    var token = req.headers["x-access-token"];
    db.get("users")
      .find({token: token})
      .set({token: null})
      .write();

    res.json({});
  },

  validate: function(username, password) {
    return db.get("users")
      .find({username: username, password: password})
      .value();
  },

  validateUser: function(token) {
    try {
      var obj = jwt.decode(token, getSecretKey());

      if(obj) {
        var username = obj.username;

        return db.get("users")
          .find({username: username})
          .value();
      } else {
        return null;
      }
    } catch(err) {
      return null;
    }
  }
}

function genToken(user) {
  return jwt.encode({
    username: user.username
  }, getSecretKey());
}

module.exports = auth;