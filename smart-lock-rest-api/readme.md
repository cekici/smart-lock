Smart Lock Rest API
===================
REST API for SmartLock.
It is a dummy fake API developed to be consumed by the SmartLock Web App.

Node v8.x & npm v5.x is required

## How To Build
```
$ npm install
```

## How To Run
```
$ node server.js
```

Then the REST API is served on [http://localhost:3000](http://localhost:3000).

## How To Use
On this application, [lowdb](https://github.com/typicode/lowdb) is used as a small JSON database. When the app first initialized, `db.json` file is created on root path and the DB state would be persisted
for any further use. For the sake of simplicity, an initial admin user is created as `username`: `admin` and password: `123`.

A simple authentication & authorization mechanism is defined on `/middlewares/validateRequest.js`. Authentication is done by creating a simple token after each successful login of any user
and token will be deleted on user logout. Authorization is designed as user role levels which are simply `admin` and `staff`. The initial admin user has the role admin and
any new user created on the app is `staff`. Each user on the system has the password: `123`.

After each user or door creation, related user-to-doors or door-to-users authorization data is created as false. Which can be updated further by running `update` method of authorization model.

After each request made to `open door` endpoint, related log is stored in the database.

You can see all routes on `/routes/index.js` file.
