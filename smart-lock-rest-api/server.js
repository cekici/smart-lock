var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var shortid = require('shortid');
var dbModule = require('./db');

var db = dbModule.create();
var users = db.get('users');
var admin = users.find({username: "admin"}).value();

if (!admin) {
  users
    .push({
      id: shortid.generate(),
      username: "admin",
      password: "123",
      firstName: "Admin",
      lastName: "TheCloudLock",
      role: "admin"
    })
    .write();
}

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());

app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});


app.all('/api/*', [require('./middlewares/validateRequest')]);
app.use('/', require('./routes'));

app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Start the server
app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + server.address().port);
});