var low = require('lowdb');
var FileSync = require('lowdb/adapters/FileSync');

var db;

var dbModule = {
  get: function () {
    return db;
  },
  create: function () {
    var adapter = new FileSync('./db.json');
    db = low(adapter);

    db.defaults({
      users: [],
      doors: [],
      authorizations: [],
      logs: []
    })
      .write();

    return db;
  }
}

module.exports = dbModule;