var jwt = require('jwt-simple');
var validateUser = require('../routes/auth').validateUser;
var getSecretKey = require('../config/secret.js');

module.exports = function (req, res, next) {
  if (req.method == 'OPTIONS') {
    next();
  } else {

    if(isAuthenticationNeeded(req)) {
      var token = req.headers['x-access-token'];

      if (token) {
        try {
          var user = validateUser(token);
          if (user) {
            if(isAuthorizationNeeded(req)) {

              if(user.role === 'admin') {
                next();
              } else {
                res.status(403);
                res.json({"message": "Not Authorized"});
              }
            } else {
              next();
            }
          } else {
            res.status(401);
            res.json({"message": "Invalid User"});
          }

        } catch (err) {
          res.status(500);
          res.json({"message": "Oops something went wrong", "error": err});
        }
      } else {
        res.status(401);
        res.json({"status": 401, "message": "Invalid Token or Key"});
      }
    } else {
      next();
    }
  }
};

function isAuthenticationNeeded(req) {
  return req.url !== "/api/login" && req.url !== "/api/me";
}

function isAuthorizationNeeded(req) {
  return (((req.method === "POST" || req.method === "PUT" || req.method === "DELETE") && req.url.indexOf("/api/door") === 0) && req.url.indexOf("/open") === -1 ||
    (req.url.indexOf("/api/users") === 0) ||
    (req.url.indexOf("/api/user") === 0) ||
    (req.url.indexOf("/api/logs") === 0) ||
    (req.url.indexOf("/api/door-authentications") === 0) ||
    (req.url.indexOf("/api/door-authentication") === 0)
  );
}